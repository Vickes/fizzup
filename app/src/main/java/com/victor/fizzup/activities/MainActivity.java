package com.victor.fizzup.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;

import com.victor.fizzup.R;

import java.util.List;

public class MainActivity extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Liste des exercices");

        JsonTransform process = new JsonTransform(getApplicationContext(),this, "https://s3-us-west-1.amazonaws.com/fizzup/files/public/sample.json");
        process.execute();


    }
}
