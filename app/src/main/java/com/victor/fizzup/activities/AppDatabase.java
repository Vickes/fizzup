package com.victor.fizzup.activities;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.victor.fizzup.activities.DAO.ExerciseDao;

@Database(entities = {Exercise.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ExerciseDao exerciseDao();
}
