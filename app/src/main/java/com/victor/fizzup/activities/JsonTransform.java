package com.victor.fizzup.activities;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.room.Room;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.spec.ECField;
import java.util.ArrayList;


public class JsonTransform extends AsyncTask<Void,Void,Void>{
    String data = "";
    private Context context;
    private Activity activity;
    ArrayList<Exercise> exercises = new ArrayList<Exercise>();
    private String urlJson="";

    public JsonTransform (Context mcontext, Activity mactivity, String murl){
        context = mcontext;
        activity = mactivity;
        urlJson = murl;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url = new URL(urlJson);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while(line != null){
                line = bufferedReader.readLine();
                data = data + line;
            }

            JSONObject ob = new JSONObject(data);
            JSONArray arr = ob.getJSONArray("data");


            for(int i = 0;i < arr.length(); i++){
                JSONObject JO = (JSONObject) arr.get(i);
                Exercise ex = new Exercise();
                ex.setId((int) JO.get("id"));
                ex.setName((String) JO.get("name"));
                ex.setImageUrl((String) JO.get("image_url"));
                exercises.add(ex);

            }

            AppDatabase db = Room.databaseBuilder(context,
                    AppDatabase.class, "database.db").build();
            for(Exercise exercise : exercises){
                db.exerciseDao().insertExercise(exercise);
            }
            db.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        LoadExercises process = new LoadExercises(context, activity);
        process.execute();
        // Change UI from progressbar activity to list of exercise activity
    }
}

