package com.victor.fizzup.activities;

import android.content.ContentValues;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.OnConflictStrategy;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.victor.fizzup.activities.DAO.ExerciseDao;

@androidx.room.Database(entities = {Exercise.class}, version = 1, exportSchema = false)
public abstract class Database extends RoomDatabase {
    private static volatile Database INSTANCE;

    public abstract ExerciseDao exerciseDao();

    public static Database getInstance(Context context){
        if(INSTANCE == null){
            synchronized (Database.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), Database.class,"FizzUp.db")
                            .addCallback(prepopulateDatabase())
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static Callback prepopulateDatabase(){
        return new Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);

                ContentValues contentValues = new ContentValues();
                contentValues.put("id",1);
                contentValues.put("name","Test name");
                contentValues.put("imageUrl","https://duajoi2hx0yfn.cloudfront.net/138/exercises/1400148535705_1.png");

                db.insert("Exercice", OnConflictStrategy.IGNORE, contentValues);
            }
        };
    }
}
