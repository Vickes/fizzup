package com.victor.fizzup.activities;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.victor.fizzup.R;

import java.util.List;

public class LoadExercises extends AsyncTask<Void,Void,Void> {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ExerciseAdapter exerciseAdapter;
    private Context context;
    private Activity activity;

    public LoadExercises (Context mcontext, Activity mactivity){
        context = mcontext;
        activity = mactivity;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        recyclerView = activity.findViewById(R.id.exercisesRecyclerView);
        layoutManager = new LinearLayoutManager(context);


        AppDatabase db = Room.databaseBuilder(context,
                AppDatabase.class, "database.db").build();

        List<Exercise> listExercise = db.exerciseDao().getExercises();
        db.close();
        exerciseAdapter = new ExerciseAdapter(listExercise);



        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(exerciseAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(context,
                DividerItemDecoration.VERTICAL));
        activity.findViewById(R.id.progress_circular).setVisibility(View.INVISIBLE);
    }
}
